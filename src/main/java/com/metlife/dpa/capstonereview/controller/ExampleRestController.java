package com.metlife.dpa.capstonereview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.capstonereview.model.Player;
import com.metlife.dpa.capstonereview.repository.PlayerRepository;

@RestController
public class ExampleRestController {

	@Autowired
	PlayerRepository playerRepository;
	
	@RequestMapping("/players")
	public List<Player> players(){
		return playerRepository.findAll();
	}
	
	@RequestMapping(value = "/players", method = RequestMethod.POST)
	public void addPlayer(String name, String postion){
		
		Player player = new Player(name, postion);
		
		playerRepository.save(player);
	}
	
	@RequestMapping("/players/{id}")
	public Player getPlayer(@PathVariable String id){
		
		return playerRepository.findOne(id);
	}
	
	@RequestMapping(value="/seedplayers", method=RequestMethod.POST)
	public void seedPlayers(){
		List<Player> players = new ArrayList<Player>();
		players.add(new Player("Derrick Turner", "Quarter Back"));
		players.add(new Player("Sam Beard", "Tight End"));
		players.add(new Player("Brian O'Connor", "Corner Back"));
		players.add(new Player("Amanda Habib", "Running Back"));
		players.add(new Player("Jessica Rex", "Wide Reciever"));
		players.add(new Player("Emily Petrilla", "Quarter Back"));
		players.add(new Player("Patrick Register", "Corner Back"));
		players.add(new Player("Nick Albrecht", "Center"));
		players.add(new Player("Clint Fitch", "Quarter Back"));
		players.add(new Player("Rob Wood", "Full Back"));
		
		playerRepository.save(players);
	}
}
