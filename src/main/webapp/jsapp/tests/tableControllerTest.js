describe("Table Controller", function(){
       var tableController;
       var scope;
       var players = [
           {name: "Derrick", position: "RB"},
           {name: "Brian", position:"Safety"}
       ];
    
    beforeEach(function(){
       module("mcApp");
    
    inject(function($controller, $rootScope){
        scope = $rootScope.$new();
        customerController  = $controller("tableCtrl", {
            $scope: scope,
            players: players
        })
    })
    });
    
    it("should have at least one player", function() {
        expect(scope.players.length).toBeGreaterThan(0);
    });
})

    