describe("Player Controller", function(){
       var playerController;
       var scope;
       var player ={name: "Derrick", position: "RB"};
    
    beforeEach(function(){
       module("mcApp");
    
    inject(function($controller, $rootScope){
        scope = $rootScope.$new();
        customerController  = $controller("playerCtrl", {
            $scope: scope,
            player: player
        })
    })
    });
    
    it("should be Derrick", function() {
        expect(scope.player.name).toBe("Derrick");
    });
})

    