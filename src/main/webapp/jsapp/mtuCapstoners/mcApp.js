'use strict';

// Declare app level module which depends on views, and components
var mcApp = angular.module('mcApp', ['ngRoute', 'ngResource']);


mcApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/',
			{
				templateUrl: '/capstone-review/jsapp/mtuCapstoners/templates/main.tpl.html',
				controller: 'mainCtrl'
			}
	)
    
    .when('/main/',
          {
				templateUrl: '/capstone-review/jsapp/mtuCapstoners/templates/main.tpl.html',
				controller: 'mainCtrl'
			}
	)
		
    .when('/player/:playerId',
          {
				templateUrl: '/capstone-review/jsapp/mtuCapstoners/templates/player.tpl.html',
				controller: 'playerCtrl',
				resolve: {
					player:function(mcIndividualPlayerService, $routeParams){
                        console.log("Player ID is: " + $routeParams.playerId);
						return mcIndividualPlayerService.getPlayer($routeParams.playerId);
					}
				}
			}
	)
    
    .when('/table/',
          {
				templateUrl: '/capstone-review/jsapp/mtuCapstoners/templates/table.tpl.html',
				controller: 'tableCtrl',
				resolve: {
					players:function(mcPlayerService){
						return mcPlayerService.getPlayers();
					}
				}
			}
	)
    
    .when('/addPlayer/',
          {
				templateUrl: '/capstone-review/jsapp/mtuCapstoners/templates/addPlayer.tpl.html',
				controller: 'addPlayerCtrl',
				resolve: {
					add:function(mcAddPlayerService){
                        return mcAddPlayerService.postPlayer;
					}
				}
			}
	);
    
	$routeProvider.otherwise({redirectTo: '/'});
}]);
