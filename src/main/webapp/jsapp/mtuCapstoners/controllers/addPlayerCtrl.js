mcApp.controller('addPlayerCtrl', ["$scope","add",function($scope, add) {
    $scope.player ={};
    $scope.positions = [
        "Quarter Back",
        "Running Back",
        "Corner Back",
        "Tight End",
        "Wide Receiver",
        "Safety",
        "Center",
        "Kicker",
        "Left Tackle",
        "Right Tackle",
    ];
   $scope.save = function(){
        add($scope.player);
    };
}]);